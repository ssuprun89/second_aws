REGION = 'us-east-1'
POST (/jobs) <br>
1. Request {"url":[url, url]}
2. Response [{'site': 'url'', 'time': time, 'status': status}]

GET (/jobs)
1. Request {}
2. Response {
  "SUCCEEDED": [
    "arn:aws:states:us-east-1:938668680897:execution:myStateMachine:3f3995e0-1519-11eb-bfca-95e246c20dae"]}

GET (/jobs/{jobs_id})
1. Request {}, url= /jobs/arn:aws:states:us-east-1:938668680897:execution....
2. Response {
  "executionArn": "arn:aws:states:us-east-1:938668680897:execution:myStateMachine:3f3995e0-1519-11eb-bfca-95e246c20dae",
  "stateMachineArn": "arn:aws:states:us-east-1:938668680897:stateMachine:myStateMachine",....}
  
GET (/website)
1. Request {}
2. Response [
  {
    "name": {
      "S": "www.feedforall.com"
    },
    "website_id": {
      "N": "1603448375"
    }
  }]
  
GET (/website/{website_id})
1. Request {}, url= /website/1603448375
2. Response [
  {
    "published": "Tue, 19 Oct 2004 11:08:57 -0400",
    "feeds_id": "3fa35451-1519-11eb-8cc8-53e513af3265",
    "website_id": "1603448375",
    "website_url": "https://www.feedforall.com/sample.xml",
    "link": "http://www.feedforall.com/banks.htm",
    "type": "site",
    "title": "RSS Solutions for Banks / Mortgage Companies"
  }]