import grequests
import json
import boto3
import uuid
from urllib.parse import urlparse


def func_except(resp, e, list_resp):
    if e:
        list_resp.append({'site': resp.url, 'time': 0, 'status': 'Connection Refused'})
    else:
        list_resp.append({'site': resp.url, 'time': 0, 'status': 'TimeOut'})


def lambda_handler(event, context):
    list_resp = []
    body_request = json.loads(event['body'])
    # body_request = event
    list_url = body_request['url']
    client = boto3.client('stepfunctions')
    rs = (grequests.get(u) for u in list_url)
    for r in grequests.map(rs, gtimeout=5, size=16, exception_handler=lambda resp, e: func_except(resp, e, list_resp)):
        if r:
            sec_resp = r.elapsed.total_seconds()
            status = 'Done!'
            list_resp.append({'site': r.url, 'time': sec_resp, 'status': status})
            domain = urlparse(r.url).netloc
            if domain == 'twitter.com':
                type_url = 'twitter'
            else:
                type_url = 'site'
            data = {"type": type_url, 'url': r.url}
            client.start_execution(
                stateMachineArn='arn:aws:states:us-east-1:938668680897:stateMachine:myStateMachine',
                name=str(uuid.uuid1()),
                input=json.dumps(data)
            )

    return {
        "statusCode": 200,
        "body": json.dumps(list_resp),
        "headers": {
            "Content-Type": "application/json"
        }
    }