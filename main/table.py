import datetime
import json
from urllib.parse import urlparse

import boto3
import uuid


def table_func(event, context):
    table_feeds = boto3.resource('dynamodb').Table('feeds')
    table_website = boto3.resource('dynamodb').Table('website')
    data = json.loads(event['data'])
    for row in data:
        feeds_id = str(uuid.uuid1())
        resp = table_website.scan()['Items']
        list_name = {r['name']: int(r['website_id']) for r in resp}
        website_domain = urlparse(event['website_url']).netloc
        if website_domain in list_name.keys():
            website_id = str(list_name[website_domain])
        else:
            website_id = str(datetime.datetime.now().timestamp()).split('.')[0]
            table_website.put_item(
                Item={
                    'website_id': int(website_id),
                    'name': website_domain,
                }
            )
        table_feeds.put_item(
            Item={
                'feeds_id': feeds_id,
                'type': event['type'],
                'website_url': event['website_url'],
                'website_id': website_id,
                'title': row['title'],
                'link': row['link'],
                'published': row['published']
            }
        )
    return {'db': True}
