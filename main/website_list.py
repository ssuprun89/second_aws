import json
import boto3


def website_all(event, context):
    try:
        client = boto3.client('dynamodb')
    except:
        return json.loads('{"error":"boto3"}')
    response = client.scan(
        TableName='website'
    )
    return response['Items']


def website_id(event, context):
    event = event['pathParameters']
    client = boto3.resource('dynamodb')
    table = client.Table('feeds')
    response = table.scan(
        FilterExpression='website_id = :website_id',
        ExpressionAttributeValues={":website_id": event['website_id']}
    )
    return response['Items']
