import json
import uuid

import feedparser


def lambda_handler(event, context):
    feed = feedparser.parse(event['url'])

    if feed['items']:
        headlines = []

        for idx, newsitem in enumerate(feed['items']):
            if idx > len(feed['items']) - 5:
                headlines.append({
                    'title': newsitem['title'],
                    'link': newsitem['link'],
                    'published': newsitem['published']
                })

        response = {'res': True,
                    'type': event['type'],
                    'website_url': event['url'],
                    'data': json.dumps(headlines),
                    'id': str(uuid.uuid1())}

        return response
    else:
        return {'res': False}
