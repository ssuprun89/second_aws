import json
import boto3
import datetime


def jobs_all(event, context):
    try:
        event = event['queryStringParameters']
    except:
        pass
    resp_dict = {}
    client = boto3.client('stepfunctions')
    list_status = ['SUCCEEDED', 'TIMED_OUT', 'ABORTED', 'FAILED', 'RUNNING']
    for status in list_status:

        response = client.list_executions(
            stateMachineArn='arn:aws:states:us-east-1:938668680897:stateMachine:myStateMachine',
            statusFilter=status
        )
        list_id = [execu['executionArn'] for execu in response['executions']]
        resp_dict[status] = list_id

    return resp_dict


def jobs_id(event, context):
    event = event['pathParameters']

    client = boto3.client('stepfunctions')
    response = client.describe_execution(
        executionArn=event['jobs_id']
    )
    response['startDate'] = response['startDate'].strftime('%y-%m-%d %h:%M:%s')
    response['stopDate'] = response['stopDate'].strftime('%y-%m-%d %h:%M:%s')
    return response


